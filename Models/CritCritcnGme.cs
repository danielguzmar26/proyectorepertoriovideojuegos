﻿using System;
using System.Collections.Generic;

namespace ProyectoRepertorioJuegos.Models
{
    public partial class CritCritcnGme
    {
        public int? IdCrit1 { get; set; }
        public int? IdGme3 { get; set; }
        public string Rsna { get; set; }

        public virtual Criticos IdCrit1Navigation { get; set; }
        public virtual Videojuego IdGme3Navigation { get; set; }
    }
}
