﻿using System;
using System.Collections.Generic;

namespace ProyectoRepertorioJuegos.Models
{
    public partial class Videojuego
    {
        public int IdGme { get; set; }
        public string NomGme { get; set; }
        public string GenGme { get; set; }
        public int? DurGme { get; set; }
        public int? ClaveDes1 { get; set; }

        public virtual Desarrolladora ClaveDes1Navigation { get; set; }
    }
}
