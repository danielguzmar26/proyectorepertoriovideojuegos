﻿using System;
using System.Collections.Generic;

namespace ProyectoRepertorioJuegos.Models
{
    public partial class Usuario
    {
        public int IdUsu { get; set; }
        public string NomUsu { get; set; }
        public int EdadUsu { get; set; }
    }
}
