﻿using System;
using System.Collections.Generic;

namespace ProyectoRepertorioJuegos.Models
{
    public partial class Desarrolladora
    {
        public Desarrolladora()
        {
            Videojuego = new HashSet<Videojuego>();
        }

        public int ClaveDes { get; set; }
        public string NomDes { get; set; }
        public DateTime FechaDes { get; set; }

        public virtual ICollection<Videojuego> Videojuego { get; set; }
    }
}
