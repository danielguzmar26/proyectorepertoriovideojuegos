﻿using System;
using System.Collections.Generic;

namespace ProyectoRepertorioJuegos.Models
{
    public partial class Criticos
    {
        public int IdCrit { get; set; }
        public string NomCrit { get; set; }
        public int GmecompltCrit { get; set; }
    }
}
