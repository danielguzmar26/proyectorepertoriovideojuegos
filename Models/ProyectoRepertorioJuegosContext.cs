﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ProyectoRepertorioJuegos.Models
{
    public partial class ProyectoRepertorioJuegosContext : DbContext
    {
        public ProyectoRepertorioJuegosContext()
        {
        }

        public ProyectoRepertorioJuegosContext(DbContextOptions<ProyectoRepertorioJuegosContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CritCritcnGme> CritCritcnGme { get; set; }
        public virtual DbSet<Criticos> Criticos { get; set; }
        public virtual DbSet<Desarrolladora> Desarrolladora { get; set; }
        public virtual DbSet<TieVendGme> TieVendGme { get; set; }
        public virtual DbSet<Tiendas> Tiendas { get; set; }
        public virtual DbSet<UsuCalfGme> UsuCalfGme { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<Videojuego> Videojuego { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=localhost;port=3306;user=root;database=biblioteca_de_juegos", x => x.ServerVersion("10.4.11-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CritCritcnGme>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("crit_critcn_gme");

                entity.HasIndex(e => e.IdCrit1)
                    .HasName("fk_fcrit1");

                entity.HasIndex(e => e.IdGme3)
                    .HasName("fk_fgme3");

                entity.Property(e => e.IdCrit1)
                    .HasColumnName("id_crit1")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdGme3)
                    .HasColumnName("id_gme3")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Rsna)
                    .HasColumnName("rsna")
                    .HasColumnType("varchar(1500)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.HasOne(d => d.IdCrit1Navigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdCrit1)
                    .HasConstraintName("fk_fcrit1");

                entity.HasOne(d => d.IdGme3Navigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdGme3)
                    .HasConstraintName("fk_fgme3");
            });

            modelBuilder.Entity<Criticos>(entity =>
            {
                entity.HasKey(e => e.IdCrit)
                    .HasName("PRIMARY");

                entity.ToTable("criticos");

                entity.Property(e => e.IdCrit)
                    .HasColumnName("id_crit")
                    .HasColumnType("int(11)");

                entity.Property(e => e.GmecompltCrit)
                    .HasColumnName("gmecomplt_crit")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NomCrit)
                    .IsRequired()
                    .HasColumnName("nom_crit")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<Desarrolladora>(entity =>
            {
                entity.HasKey(e => e.ClaveDes)
                    .HasName("PRIMARY");

                entity.ToTable("desarrolladora");

                entity.Property(e => e.ClaveDes)
                    .HasColumnName("clave_des")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FechaDes)
                    .HasColumnName("fecha_des")
                    .HasColumnType("datetime");

                entity.Property(e => e.NomDes)
                    .IsRequired()
                    .HasColumnName("nom_des")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<TieVendGme>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tie_vend_gme");

                entity.HasIndex(e => e.IdGme2)
                    .HasName("fk_fgme2");

                entity.HasIndex(e => e.IdTie1)
                    .HasName("fk_ftie1");

                entity.Property(e => e.IdGme2)
                    .HasColumnName("id_gme2")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdTie1)
                    .HasColumnName("id_tie1")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IdGme2Navigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdGme2)
                    .HasConstraintName("fk_fgme2");

                entity.HasOne(d => d.IdTie1Navigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdTie1)
                    .HasConstraintName("fk_ftie1");
            });

            modelBuilder.Entity<Tiendas>(entity =>
            {
                entity.HasKey(e => e.IdTie)
                    .HasName("PRIMARY");

                entity.ToTable("tiendas");

                entity.Property(e => e.IdTie)
                    .HasColumnName("id_tie")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NomSuc)
                    .IsRequired()
                    .HasColumnName("nom_suc")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.SitioTie)
                    .IsRequired()
                    .HasColumnName("sitio_tie")
                    .HasColumnType("varchar(300)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<UsuCalfGme>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("usu_calf_gme");

                entity.HasIndex(e => e.IdGme1)
                    .HasName("fk_fgme1");

                entity.HasIndex(e => e.IdUsu1)
                    .HasName("fk_fusu1");

                entity.Property(e => e.Calf)
                    .HasColumnName("calf")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdGme1)
                    .HasColumnName("id_gme1")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdUsu1)
                    .HasColumnName("id_usu1")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IdGme1Navigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdGme1)
                    .HasConstraintName("fk_fgme1");

                entity.HasOne(d => d.IdUsu1Navigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdUsu1)
                    .HasConstraintName("fk_fusu1");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.IdUsu)
                    .HasName("PRIMARY");

                entity.ToTable("usuario");

                entity.Property(e => e.IdUsu)
                    .HasColumnName("id_usu")
                    .HasColumnType("int(11)");

                entity.Property(e => e.EdadUsu)
                    .HasColumnName("edad_usu")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NomUsu)
                    .IsRequired()
                    .HasColumnName("nom_usu")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<Videojuego>(entity =>
            {
                entity.HasKey(e => e.IdGme)
                    .HasName("PRIMARY");

                entity.ToTable("videojuego");

                entity.HasIndex(e => e.ClaveDes1)
                    .HasName("fk_fdes1");

                entity.Property(e => e.IdGme)
                    .HasColumnName("id_gme")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ClaveDes1)
                    .HasColumnName("clave_des1")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DurGme)
                    .HasColumnName("dur_gme")
                    .HasColumnType("int(11)");

                entity.Property(e => e.GenGme)
                    .HasColumnName("gen_gme")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.NomGme)
                    .HasColumnName("nom_gme")
                    .HasColumnType("varchar(200)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.HasOne(d => d.ClaveDes1Navigation)
                    .WithMany(p => p.Videojuego)
                    .HasForeignKey(d => d.ClaveDes1)
                    .HasConstraintName("fk_fdes1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
