﻿using System;
using System.Collections.Generic;

namespace ProyectoRepertorioJuegos.Models
{
    public partial class Tiendas
    {
        public int IdTie { get; set; }
        public string NomSuc { get; set; }
        public string SitioTie { get; set; }
    }
}
