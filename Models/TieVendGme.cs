﻿using System;
using System.Collections.Generic;

namespace ProyectoRepertorioJuegos.Models
{
    public partial class TieVendGme
    {
        public int? IdTie1 { get; set; }
        public int? IdGme2 { get; set; }

        public virtual Videojuego IdGme2Navigation { get; set; }
        public virtual Tiendas IdTie1Navigation { get; set; }
    }
}
