﻿using System;
using System.Collections.Generic;

namespace ProyectoRepertorioJuegos.Models
{
    public partial class UsuCalfGme
    {
        public int? IdUsu1 { get; set; }
        public int? IdGme1 { get; set; }
        public int? Calf { get; set; }

        public virtual Videojuego IdGme1Navigation { get; set; }
        public virtual Usuario IdUsu1Navigation { get; set; }
    }
}
