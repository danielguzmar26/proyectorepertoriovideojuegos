﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoRepertorioJuegos.Models
{
    public partial class TieVendGme
    {
        public override string ToString()
        {
            return $"{IdTie1}{IdGme2}";
        }
    }
}
