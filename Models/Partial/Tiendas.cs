﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoRepertorioJuegos.Models
{
    public partial class Tiendas
    {
        public override string ToString()
        {
            return $"{IdTie}) {NomSuc} {SitioTie}";
        }
    }
}
