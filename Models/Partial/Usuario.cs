﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoRepertorioJuegos.Models
{
    public partial class Usuario
    {
        public override string ToString()
        {
            return $"{IdUsu}) {NomUsu}{EdadUsu}";
        }
    }
}
