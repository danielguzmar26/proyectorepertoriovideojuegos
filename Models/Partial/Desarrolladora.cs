﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoRepertorioJuegos.Models
{
    public partial class Desarrolladora
    {
        public override string ToString()
        {
            return $"{ClaveDes}) {NomDes}{FechaDes}";
        }
    }
}
