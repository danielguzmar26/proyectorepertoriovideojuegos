﻿using System;
using ProyectoRepertorioJuegos.Models;
using System.Linq;

namespace ProyectoRepertorioJuegos
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int opcion;
                do
                {
                    Console.WriteLine("Opciones");
                    Console.WriteLine("1: Usuario Nuevo");
                    Console.WriteLine("2: Critico Nuevo");
                    Console.WriteLine("3: Reseña (Solo para criticos)");
                    Console.WriteLine("4: Calificar (Solo Usuarios)");
                    Console.WriteLine("5: Nuevo videojuego");
                    Console.WriteLine("6: Consultar videojuegos");
                    Console.WriteLine("7: Ver reseñas de juegos");
                    Console.WriteLine("8: Ver calificaciones de juegos");
                    Console.WriteLine("9: Eliminar Juego");
                    Console.WriteLine("10: Agregar estudio desarrollador");
                    Console.WriteLine("");
                    Console.Write("Seleccion: ");
                    opcion = int.Parse(Console.ReadLine());
                    Console.WriteLine("");

                    switch (opcion)
                    {
                        case 1:
                            AgregarUsuario();
                            break;
                        case 2:
                            AgregarCritico();
                            break;
                        case 3:
                            CriticarJuego();
                            break;
                        case 4:
                            CalificarJuego();
                            break;
                        case 5:
                            AgregarVideojuego();
                            break;
                        case 6:
                            MostrarVideojuego();
                            break;
                        case 7:
                            MostrarCriticas();
                            break;
                        case 8:
                            MostrarCalificaciones();
                            break;
                        case 9:
                            EliminarVideojuegos();
                            break;
                        case 10:
                            AgregarDesarrolladora();
                            break;
                        case 11:
                            MostrarDesarrolladora();
                            break;
                        default:
                            Console.WriteLine("Se ha insertado un numero inválido, intente de nuevo.");
                            break;
                        case 0:
                            Console.Write("");
                            break;
                    }
                } while (opcion != 0);
            }
            catch (Exception e)
            {
                Console.WriteLine("");
                Console.WriteLine("Opcion invalida" + e);
                Console.WriteLine("");
                Main(args);

            }
        }
        // SE INICIAN LOS METODOS DE INSERCION DE DATOS
        public static void AgregarUsuario()
        {
            try
            {
                Console.WriteLine("Nuevo Usuario: ");
                Usuario usuario = new Usuario();
                Console.Write("Nombre de usuario: ");
                usuario.NomUsu = Console.ReadLine();
                Console.Write("Edad de usuario: ");
                usuario.EdadUsu = int.Parse(Console.ReadLine());
                using (ProyectoRepertorioJuegosContext context = new ProyectoRepertorioJuegosContext())
                {
                    context.Add(usuario);
                    context.SaveChanges();
                    Console.WriteLine("");
                    Console.WriteLine("Usuario registrado");
                }
            }
            catch
            {
                Console.WriteLine("");
                Console.WriteLine("Syintax invalida");
                Console.WriteLine("");
                AgregarUsuario();

            }


        }
        public static void AgregarCritico()
        {
            try
            {
                Console.WriteLine("Nuevo Critico: ");
                Criticos criticos = new Criticos();
                Console.Write("Nombre de Critico: ");
                criticos.NomCrit = Console.ReadLine();
                Console.Write("Juegos completados: ");
                criticos.GmecompltCrit = int.Parse(Console.ReadLine());
                using (ProyectoRepertorioJuegosContext context = new ProyectoRepertorioJuegosContext())
                {
                    context.Add(criticos);
                    context.SaveChanges();
                    Console.WriteLine("");
                    Console.WriteLine("Critico registrado");
                    Console.WriteLine("");
                }
            }
            catch
            {
                Console.WriteLine("Syintax invalida");
                AgregarCritico();
            }
        }
        public static void AgregarVideojuego()
        {
            try
            {
                Console.WriteLine("Nuevo videojuego: ");
                Videojuego videojuego = new Videojuego();
                Console.Write("Nombre de videojuego: ");
                videojuego.NomGme = Console.ReadLine();
                Console.Write("Genero de videojuego: ");
                videojuego.GenGme = Console.ReadLine();
                Console.WriteLine("Duracion de videojuego: ");
                videojuego.DurGme = int.Parse(Console.ReadLine());
                Console.WriteLine("Clave del estudio desarrollador(Debe estar registrada): ");
                videojuego.ClaveDes1 = int.Parse(Console.ReadLine());
                using (ProyectoRepertorioJuegosContext context = new ProyectoRepertorioJuegosContext())
                {
                    context.Add(videojuego);
                    context.SaveChanges();
                    Console.WriteLine("");
                    Console.WriteLine("Videojuego registrado");
                    Console.WriteLine("");
                }
            }
            catch
            {
                Console.WriteLine("");
                Console.WriteLine("Syintax invalida");
                Console.WriteLine("");
                AgregarVideojuego();
            }


        }
        public static void AgregarDesarrolladora()
        {
            try
            {
                Console.WriteLine("Nuevo estudio desarrollador");
                Desarrolladora desarrolladora = new Desarrolladora();
                Console.Write("Nombre del estudio: ");
                desarrolladora.NomDes = Console.ReadLine();
                desarrolladora.FechaDes = DateTime.Now;
                using (ProyectoRepertorioJuegosContext context = new ProyectoRepertorioJuegosContext())
                {
                    context.Add(desarrolladora);
                    context.SaveChanges();
                    Console.WriteLine("");
                    Console.WriteLine("Estudio registrado");
                    Console.WriteLine("");
                }
            }
            catch
            {
                Console.WriteLine("");
                Console.WriteLine("Syintax invalida");
                Console.WriteLine("");
                AgregarVideojuego();
            }


        }
        // SE INICIAN LOS METODOS DE ELIMINACION DE DATOS
        public static void EliminarVideojuegos()
        {
            try
            {
                Console.WriteLine("Inserte el juego que desea eliminar: ");
                Videojuego videojuego = new Videojuego();

                Console.Write("Numero: ");
                videojuego.IdGme = int.Parse(Console.ReadLine());

                using (ProyectoRepertorioJuegosContext context = new ProyectoRepertorioJuegosContext())
                {
                    context.Remove(videojuego);
                    context.SaveChanges();
                    Console.WriteLine("El videojuego fue eliminado");
                }
            }
            catch
            {
                Console.WriteLine("Campos inválidos o vacíos");
                EliminarVideojuegos();
            }
        }
        // SE INICIAN METODOS DE MOSTRAR
        public static void MostrarVideojuego()
        {
            Console.WriteLine("JUEGOS REGISTRADOS");
            using (ProyectoRepertorioJuegosContext context = new ProyectoRepertorioJuegosContext())
            {
                foreach (Videojuego ventam in context.Videojuego)
                {
                    Console.WriteLine($"{ventam.IdGme}) Nombre de videojuego: {ventam.NomGme} " +
                        $"  Genero: {ventam.GenGme}    Id de estudio: {ventam.ClaveDes1}");
                }
            }
        }
        public static void MostrarDesarrolladora()
        {
            Console.WriteLine("DESARROLLADORAS REGISTRADAS");
            using (ProyectoRepertorioJuegosContext context = new ProyectoRepertorioJuegosContext())
            {
                foreach (Desarrolladora ventam in context.Desarrolladora)
                {
                    Console.WriteLine($"{ventam.ClaveDes}) Nombre de videojuego: {ventam.NomDes} Fecha de inicio {ventam.FechaDes}");
                }
            }
        }
        // SE INICIAN METODOS PARA VALORAR DATOS
        public static void CalificarJuego()

        {
            try
            {
                Console.WriteLine("CALIFICAR VIDEOJUEGO");
                UsuCalfGme usuCalfGme = new UsuCalfGme();
                Console.Write("Inserte su ID de usuario");
                usuCalfGme.IdUsu1 = int.Parse(Console.ReadLine());
                Console.WriteLine("Inserte la ID de juego para calificar");
                usuCalfGme.IdGme1 = int.Parse(Console.ReadLine());
                Console.WriteLine("Como desea calificar al juego del 1-10 ");
                usuCalfGme.Calf = int.Parse(Console.ReadLine());
                using (ProyectoRepertorioJuegosContext context = new ProyectoRepertorioJuegosContext())
                {
                    context.Add(usuCalfGme);
                    context.SaveChanges();
                    Console.WriteLine("Calificacion Agregada");
                }
            }
            catch
            {
                Console.WriteLine("Syintax invalida");
                AgregarVideojuego();
            }
        }
        public static void CriticarJuego()

        {
            try
            {
                Console.WriteLine("CRITICAR VIDEOJUEGO");
                CritCritcnGme critCritcnGme = new CritCritcnGme();
                Console.Write("Inserte su ID de critico");
                critCritcnGme.IdCrit1 = int.Parse(Console.ReadLine());
                Console.WriteLine("Inserte la ID de juego para calificar");
                critCritcnGme.IdGme3 = int.Parse(Console.ReadLine());
                Console.WriteLine("Escriba una reseña para el videojuego");
                critCritcnGme.Rsna = Console.ReadLine();
                using (ProyectoRepertorioJuegosContext context = new ProyectoRepertorioJuegosContext())
                {
                    context.Add(critCritcnGme);
                    context.SaveChanges();
                    Console.WriteLine("Reseña Agregada");
                }
            }
            catch
            {
                Console.WriteLine("Syintax invalida");
                AgregarVideojuego();
            }
        }
        //SE INICIAN METODOS PARA MOSTRAR VALORACIONES
        public static void MostrarCalificaciones()
        {
            Console.WriteLine("Inserte el ID del juego: ");
            int Juego = int.Parse(Console.ReadLine());

            using (ProyectoRepertorioJuegosContext context = new ProyectoRepertorioJuegosContext())
            {
                IQueryable<UsuCalfGme> ventams = context.UsuCalfGme.Where(vm => vm.IdGme1.Equals(Juego));
                foreach (UsuCalfGme ventam in ventams)
                {
                    Console.WriteLine($"Consulta realizada con exito: ");
                    Console.WriteLine($"{ventam.IdGme1}) Usuario: {ventam.IdUsu1} " +
                        $"| Calificacion: {ventam.Calf}");
                }
            }
        }
        public static void MostrarCriticas()
        {
            Console.WriteLine("Inserte el ID del juego: ");
            int Juego = int.Parse(Console.ReadLine());

            using (ProyectoRepertorioJuegosContext context = new ProyectoRepertorioJuegosContext())
            {
                IQueryable<CritCritcnGme> ventams = context.CritCritcnGme.Where(vm => vm.IdGme3.Equals(Juego));
                foreach (CritCritcnGme ventam in ventams)
                {
                    Console.WriteLine($"Consulta realizada con exito: ");
                    Console.WriteLine($"{ventam.IdGme3}) Critico: {ventam.IdCrit1} " +
                        $"| Reseña: {ventam.Rsna}");
                }
            }
        }
    }
}
